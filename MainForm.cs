﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Hecsit.Oop.Intoduction.Shapes;
using Rectangle = Hecsit.Oop.Intoduction.Shapes.Rectangle;

namespace Hecsit.Oop.Intoduction
{
	public partial class MainForm : Form
	{
		private IShape _captured = null;
		private Point _mouse;
	    private List<IShape> _shapes = new List<IShape>
	    {
            new CompositeShape(
                new Point(0, 0),
                new Rectangle(new Point(50, 100), new Size(200, 25), Color.LightSteelBlue),
                new Rectangle(new Point(60, 125), new Size(20, 75), Color.LightSteelBlue),
                new Rectangle(new Point(220, 125), new Size(20, 75), Color.LightSteelBlue)),
            new Circle(new Point(100, 75), 25, Color.ForestGreen),
            new Circle(new Point(350, 100), 50, Color.Gold),
            new CompositeShape(
                new Point(300, 20),
                new Rectangle(new Point(0, 25), new Size(100, 35), Color.DarkRed),
                new Rectangle(new Point(25, 0), new Size(50, 35), Color.DarkRed))
        };
		
		public MainForm()
		{
		    InitializeComponent();
		}

	    private void MainForm_Paint(object sender, PaintEventArgs e)
		{
		    foreach (var shape in _shapes)
		    {
                shape.Render(e.Graphics);
            }
		}

		private void MainForm_MouseDown(object sender, MouseEventArgs e)
		{
			_mouse.X = e.X;
			_mouse.Y = e.Y;

            foreach (var shape in _shapes)
            {
                if (shape.Contains(e.X, e.Y))
                {
                    _captured = shape;
                }
            }
        }

		private void MainForm_MouseMove(object sender, MouseEventArgs e)
		{
			if (_captured == null) return;

            _captured.Move(e.X - _mouse.X, e.Y - _mouse.Y);
			
			_mouse.X = e.X;
			_mouse.Y = e.Y;
			Invalidate();
		}

		private void MainForm_MouseUp(object sender, MouseEventArgs e)
		{
			_captured = null;
		}
	}
}
