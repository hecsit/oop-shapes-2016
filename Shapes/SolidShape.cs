﻿using System.Drawing;

namespace Hecsit.Oop.Intoduction.Shapes
{
    public abstract class SolidShape : IShape
    {
        private readonly Brush _brush;

        protected Brush Brush
        {
            get { return _brush; }
        }

        protected SolidShape(Color color)
        {
            _brush = new SolidBrush(color);
        }

        public abstract void Render(Graphics canvas);
        public abstract bool Contains(int x, int y);
        public abstract void Move(int offsetX, int offsetY);
    }
}