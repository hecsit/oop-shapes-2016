﻿using System.Drawing;

namespace Hecsit.Oop.Intoduction.Shapes
{
    public class Rectangle : SolidShape
    {
        private Point _topLeft;
        private Size _size;

        public Rectangle(Point topLeft, Size size, Color color) 
            : base(color)
        {
            _topLeft = topLeft;
            _size = size;
        }

        public override void Render(Graphics canvas)
        {
            canvas.FillRectangle(
                Brush, 
                _topLeft.X, _topLeft.Y, 
                _size.Width, _size.Height);
        }

        public override bool Contains(int x, int y)
        {
            return
                x >= _topLeft.X && x <= _topLeft.X + _size.Width
                && y >= _topLeft.Y && y <= _topLeft.Y + _size.Height;
        }

        public override void Move(int offsetX, int offsetY)
        {
            _topLeft.X += offsetX;
            _topLeft.Y += offsetY;
        }
    }
}