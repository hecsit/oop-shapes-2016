﻿using System.Drawing;

namespace Hecsit.Oop.Intoduction.Shapes
{
    public class Circle : SolidShape
    {
        private Point _center;
        private int _radius;

        public Circle(Point center, int radius, Color color)
            : base(color)
        {
            _center = center;
            _radius = radius;
        }

        public override void Render(Graphics canvas)
        {
            canvas.FillEllipse(
                Brush, 
                _center.X - _radius, _center.Y - _radius, 
                2* _radius, 2* _radius);
        }

        public override bool Contains(int x, int y)
        {
            return (x - _center.X)*(x - _center.X)
                   + (y - _center.Y)*(y - _center.Y) 
                   <= _radius*_radius;
        }

        public override void Move(int offsetX, int offsetY)
        {
            _center.X += offsetX;
            _center.Y += offsetY;
        }
    }
}