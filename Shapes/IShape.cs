﻿using System.Drawing;

namespace Hecsit.Oop.Intoduction.Shapes
{
    public interface IShape
    {
        void Render(Graphics canvas);
        bool Contains(int x, int y);
        void Move(int offsetX, int offsetY);
    }
}