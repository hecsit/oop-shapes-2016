﻿using System.Collections.Generic;
using System.Drawing;

namespace Hecsit.Oop.Intoduction.Shapes
{
    public class CompositeShape : IShape
    {
        private Point _base;
        private readonly List<IShape> _shapes = new List<IShape>();

        public CompositeShape(Point @base, params IShape[] shapes)
        {
            _base = @base;
            _shapes.AddRange(shapes);
        }

        public void Render(Graphics canvas)
        {
            canvas.TranslateTransform(_base.X, _base.Y);
            foreach (var shape in _shapes)
            {
                shape.Render(canvas);
            }
            canvas.TranslateTransform(-_base.X, -_base.Y);
        }

        public bool Contains(int x, int y)
        {
            var localX = x - _base.X;
            var localY = y - _base.Y;

            foreach (var shape in _shapes)
            {
                if (shape.Contains(localX, localY)) return true;
            }
            return false;
        }

        public void Move(int offsetX, int offsetY)
        {
            _base.X += offsetX;
            _base.Y += offsetY;
        }
    }
}